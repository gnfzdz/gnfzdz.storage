# Ansible Role - gnfzdz.storage.snapper

Install snapper and configure btrfs subvolume snapshot hooks and policies


## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`storage_snapper_boot_snapshots` | A flag controlling whether snapper will automatically create snapshots of the root configuration on system boot | bool | True
`storage_snapper_pacman_snapshots` | A flag controlling whether snapper will automatically create pre/post snapshots of the root configuration before/after pacman transactions | bool | True
`storage_snapper_scheduled_snapshots` | A flag controlling whether snapper will automatically create snapshots on a schedule for all configurations (based on the policy defined in the snapper configuration file) | bool | True
`storage_snapper_scheduled_cleanup` | A flag controlling whether snapper will automatically prune old snapshots on a schedule for all configurations (based on the policy defined in the snapper configuration file) | bool | True
`storage_snapper_default_options` | Contains default configuration settings to use for all snapper configurations. See `man 5 snapper-configs` for a description of available settings. | {}
`storage_snapper_configs_auto` | A list of snapper configuration objects. See below for the structure | list of dict | a list containing the contents of all variables matching `/^storage_snapper_config_.*$/`


Each snapper configuration object may have the below fields:

Variable | Description | Type | Required
-------- | ----------- | -------- | --------
`name` | The name of the snapper configuration | str | True
`subvolume` | The absolute path to where the target subvolume is mounted | str | True
`options` | Contains configuration settings specific to this snapper configuration. Any setting not provided will default to the value in `storage_snapper_default_options` or otherwise the snapper default. See `man 5 snapper-configs` for all available settings. | dict | False
