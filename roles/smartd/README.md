# Ansible Role - gnfzdz.storage.smartd

Use the Smart Disk Monitoring Daemon to monitor the health of local hard disks and notify on failures

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`storage_smartd_enabled` | A flag controlling whether the smart daemon should automatically be started on system boot | bool | True if the host is installed on bare metal else False
`storage_smartd_state` | Controls the current state of the smartd daemon. Supports  | str | 'started' if storage_smartd_enabled else 'stopped'
