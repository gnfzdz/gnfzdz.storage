# Ansible Role - gnfzdz.storage.btrfs

Install support for the btrfs filesystem, create an opinionated subvolume hierarchy for root and configure periodic scrubbing of the filesystem. 

The created subvolume hierarchy is based on recommendations from the [ArchWiki](https://wiki.archlinux.org/title/snapper) and [this forum thread](https://bbs.archlinux.org/viewtopic.php?id=194491). The goal is to create a relatively flat hierarchy that should remain flat even if rollbacks occur.

Subvolume | Mountpoint | Description
-------- | ----------- | --------
`@` | `/` | The default subvolume mounted as the system's root. Notably NOT the btrfs filesystem root which is the parent to the other subvolumes listed below. The btrfs filesystem root ca not be replaced (ex: when attempting to rollback) and would also expose all child subvolumes as if they were directories under /.
`@home` | `/home` | A subvolume containing user home directories so that they can be snapshotted independent of system configuration
`@snapshots` | `/.snapshots` | A parent subvolume for all snapshots automatically managed by snapper
`@var_log` | `/var/log` | Created as a separate subvolume as the system will need to write here even when booting from read only snapshots [see grub-btrfs](https://github.com/Antynea/grub-btrfs)
`@var_tmp` | `/var/tmp` | Created as a separate subvolume to allow writing when mounting read only root snapshots
`@var_cache_pacman_pkg` | `/var/cache/pacman/pkg` | Created as a separate subvolume to allow pacman cache management independent of snapshots

## Variables
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`storage_btrfs_subvolumes` | A list of subvolumes to automatically create and mount. Each subvolume should have a name (representing the subvolume's path relative to the the filesystem's root subvolume) and a mountpoint (relative to the current root) | list of dict | [{ 'name': "@var_log", 'mountpoint': "/var/log" }, { 'name': "@snapshots", 'mountpoint': "/.snapshots" }, { 'name': "@home", 'mountpoint': "/home" }, { 'name': "@var_tmp", 'mountpoint': "/var/tmp" }, { 'name': "@var_cache_pacman_pkg", 'mountpoint': "/var/cache/pacman/pkg" }]
`storage_btrfs_default_subvolumes` | A list of subvolumes containing the default contents of `storage_btrfs_subvolumes`. Useful in scenarios where you may want to add additional subvolumes while also retaining the defaults. | list of dict | See `storage_btrfs_subvolumes`
`storage_btrfs_scrub_ratelimit_enabled` | A flag indicating whether btrfs scrubbing should be ratelimited (primarily applicable when devices do not) | bool | conditional based on ansible_form_factor
`storage_btrfs_scrub_ratelimit_bandwidth` | Configures the per device max bandwidth when scrub ratelimiting is enabled. | str | 100M
