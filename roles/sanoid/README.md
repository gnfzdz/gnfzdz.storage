# Ansible Role - gnfzdz.storage.sanoid

Install sanoid and configure zfs dataset snapshotting and send/receive policies

## Variables
-------

### Standard Variables

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`storage_sanoid_conf_template` | The path to a template file. The default sanoid template supports a range of variables documented below, but you may provide a custom template to better match your personal needs. | str | sanoid.conf.j2
`storage_sanoid_timer_enabled` | A flag indicating whether a timer should be created automating snapshot creation, cleanup and monitoring | bool | True

### Variables used by the default template
-------

Variable | Description | Type | Default
-------- | ----------- | -------- | --------
`storage_sanoid_datasets` | An array of configurations defining the snapshot, cleanup and monitoring policy for a single dataset (and optionally children) | array of dict | A list containing the contents of all variables matching `/^storage_sanoid_dataset_.*$/`
`storage_sanoid_templates` | An array of named, reusable configurations that can serve as the basis for one or many dataset configurations. | array of dict | A list containing the contents of all variables matching `/^storage_sanoid_template_.*$/` Several templates are provided by default. [See here for details](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/sanoid/defaults/main.yml)

### Fields available in dataset or template configurations
-------
Each dataset or template provided can set any configuration options supported by the sanoid configuration format. Sanoid supports a large number of configuration options for datasets and/or templates. See the [Sanoid wiki](https://github.com/jimsalterjrs/sanoid/wiki/Sanoid) for examples or the [configuration defaults](https://github.com/jimsalterjrs/sanoid/blob/master/sanoid.defaults.conf) for the full set of options. In addition to these options, the two below options are supported.

Variable | Description | Type | Required
-------- | ----------- | -------- | --------
`__name` | The name of the template or full name of the dataset | str | True
`__comment` | A comment prepended to the target configuration section | str | False
