# Ansible Collection - gnfzdz.storage

A collection of roles to support in managing locally stored data including monitoring, filesystem configuration/management and data back up

## Roles

Name | Description
-------- | -----------
[gnfzdz.storage.all](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/all/README.md) | A meta role automatically applying all configuration within this collection
[gnfzdz.storage.btrfs](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/btrfs/README.md) | Install support for the btrfs filesystem, create an opinionated subvolume hierarchy for root and install/configure supporting utilities
[gnfzdz.storage.restic](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/restic/README.md) | Install restic and configure automatic backups
[gnfzdz.storage.sanoid](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/smartd/README.md) | Install sanoid and configure zfs dataset snapshotting and send/receive policies
[gnfzdz.storage.smartd](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/smartd/README.md) | Use the Smart Disk Monitoring Daemon to monitor the health of local hard disks and notify on failures
[gnfzdz.storage.snapper](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/snapper/README.md) | Install snapper and configure btrfs subvolume snapshot hooks and policies
[gnfzdz.storage.utilities](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/utilities/README.md) | Install common utilities used to help manage files and filesystems
[gnfzdz.storage.zfs](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/roles/zfs/README.md) | Install support for the zfs filesystem, automatically create configured pools/datasets and install/configure supporting utilities

## Modules

Name | Description
-------- | -----------
[gnfzdz.storage.btrfs_info](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/plugins/modules/btrfs_info.py) | Query the current state of btrfs filesystems available
[gnfzdz.storage.btrfs_subvolume](https://gitlab.com/gnfzdz/gnfzdz.storage/-/blob/main/plugins/modules/btrfs_subvolume.py) | Manage btrfs subvolumes and snapshots
